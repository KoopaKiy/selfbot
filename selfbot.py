import discord
import asyncio
from random import choice as randchoice
import json
from pprint import pprint
import random
import datetime
import re
import colorama
from colorama import Fore
colorama.init(autoreset=True)

import config

client = discord.Client()

if __name__ == '__main__':
	global tags
	with open('files/text.json') as r:
		data = json.load(r)
	tags = data

async def say(message, msg):
	if message.author == client.user:
		await client.edit_message(message, msg)

def log(message, command):
	if message.channel.is_private:
		print(Fore.MAGENTA + message.author.name
		+ Fore.RESET + ' used the command '
		+ Fore.CYAN + command
		+ Fore.RESET + ' in '
		+ Fore.RED + 'private messages')
	else: 
		print(Fore.MAGENTA + message.author.name
		+ Fore.RESET + ' used the command '
		+ Fore.CYAN + command
		+ Fore.RESET + ' in '
		+ Fore.RED + message.server.name
		+ Fore.RESET + '/'
		+ Fore.RED + message.channel.name)

async def handletag(message, content, server):
	if content.lower().startswith('add'):
		content = content[3:].strip()

		if content.lower().startswith('global'):
			server = 'all'
			content = content[6:].strip()

		await tagadd(message, server, content)
		return

	if content.lower().startswith('rem'):
		content = content[3:].strip()

		if content.lower().startswith('global'):
			server = 'all'
			content = content[6:].strip()

		await tagrem(message, server, content)
		return


	if content.lower().startswith('list'):

		await taglist(message, server)
		return

	else:
		await gettag(message, server, content)

async def tagadd(message, server, content):
	if server not in tags:
		tags[server] = []

	info = content.split(';')
	tag = info[0].strip().lower()
	info.remove(tag)
	response = " ".join(info)

	newtag = {'name': tag, 'response': response}
	tags[server].append(newtag)
	with open('files/text.json', 'w') as w:
		json.dump(tags, w)

	await say(message, ":thumbsup::skin-tone-1: Successfully added `{}`".format(tag))
	log(message, "add {}".format(tag))
	return 

async def tagrem(message, server, content):
	global tags
	if server not in tags:
		await say(message, ":warning: I don't see this server in my tags.")
		return

	for i in tags[server]:
		if i['name'] == content:
			tags[server].remove(i)
			with open('files/text.json', 'w') as w:
				json.dump(tags, w)
			await say(message, ":thumbsup::skin-tone-1: Deleted `{}`".format(content.lower()))
			log(message, "rem {}".format(content.lower()))
			return

	await say(message, ":warning: I'm sorry, I couldn't find `{}`".format(content))
	log(message, "rem {} -NOT FOUND".format(content.lower()))

async def gettag(message, server, content):
	for i in tags['all']:
		if i['name'].lower() == content.lower():
			await say(message, i['response'])
			log(message, "tag {}".format(content.lower()))
			return

	if server in tags:
		for i in tags[server]:
			if i['name'].lower() == content.lower():
				await say(message, i['response'])
				log(message, "tag {}".format(content.lower()))
				return

	await say(message, ":warning: I couldn't seem to find that tag, sorry!")
	log(message, "{} -NOT FOUND".format(content.lower()))

async def taglist(message, server):
	msg = []

	if server in tags:
		taglist = []
		for i in tags[server]:
			taglist.append(i['name'])
		taglist = ", ".join(taglist)

		if message.channel.is_private:
			msg.append("**{}**".format(message.channel.user.name))
		else:
			msg.append("**{}**".format(message.server.name))
		msg.append(taglist)

	taglist = []
	for i in tags['all']:
		taglist.append(i['name'])
	taglist = ", ".join(taglist)
	msg.append("**Global**")
	msg.append(taglist)

	msg = "\n".join(msg)

	await say(message, msg)
	log(message, "taglist")


async def selfeval(message, code):
	if "print(" in code:
		result = "Printing your data"
	else:
		result = None
	try: 
		result = eval(code)
	except:
		try:
			exec(code)
		except Exception as e:
			await client.send_message(message.channel, "{}: {}".format(type(e).__name__, e))
		if asyncio.iscoroutine(result):
			result = await result
	await client.edit_message(message, "```python\nInput: {}\nOutput: {}\n```".format(code, result))
	log(message, code)

async def discrimfinder(message, params):
	matches = []
	members = client.get_all_members()
	for m in members:
		if m.discriminator == params and m != client.user:
			if m.name not in matches:
				matches.append(m.name)

	matches = "\n".join(matches)
	await say(message, "I found these users for discrim {}:\n{}".format(params, matches))
	log(message, "discrim " + params)

async def changediscrim(message):
	matches = []
	members = client.get_all_members()
	for m in members:
		if m.discriminator == client.user.discriminator and m != client.user:
			if m.name not in matches:
				matches.append(m.name)

	try:
		await client.edit_profile(password=config.password, username=randchoice(matches))
		await asyncio.sleep(1)
		await client.edit_profile(password=config.password, username=config.username)
		await say(message, "New discrim: " + client.user.discriminator)
	except Exception:
		await say(message, "Sorry boss, couldn't change your discrim.")
	log(message, 'changediscrim')

async def snailpurge(message, params):
	counter = 0
	params = int(params)
	await client.edit_message(message, "Purging {}".format(params))
	async for m in client.logs_from(message.channel):
		if counter <= params:
			if m.author == client.user and counter != 0:
				await client.delete_message(m)
				await asyncio.sleep(.21)
			counter += 1

	await client.edit_message(message, "Purged {}".format(params))
	await asyncio.sleep(3)
	log(message, "purge {}".format(params))
	await client.delete_message(message)


async def autochangediscrim():
	await client.wait_until_ready()
	print("Change discrim start")
	logchan = client.get_channel(config.logchan)

	while not client.is_closed:
		if client.user.discriminator in config.wanteddiscrim:
			await client.send_message(logchan, "Already on wanted discrim: " + client.user.discriminator)
			return

		matches = []
		members = client.get_all_members()
		for m in members:
			if m.discriminator == client.user.discriminator and m != client.user:
				if m.name not in matches:
					matches.append(m.name)

		try:
			await client.edit_profile(password=config.password, username=randchoice(matches))
			await asyncio.sleep(1)
			await client.edit_profile(password=config.password, username=config.username)
			await client.send_message(logchan, "New discrim: " + client.user.discriminator)
		except Exception:
			await client.send_message(logchan, "Sorry boss, couldn't change your discrim.")

		print("Change discrim end")
		await asyncio.sleep(3600)

async def gameset():
	await client.wait_until_ready()
	name = randchoice(config.games)
	game = discord.Game(name=name)
	while True:
		await client.change_status(game=game)
		await asyncio.sleep(120)


@client.event
async def on_ready():
	logchan = client.get_channel(config.logchan)
	print("Authenticated as: {}".format(client.user.name))
	loop = asyncio.get_event_loop()
	loop.create_task(gameset())
	# Putting a hold on the discrim finding. Remove the # to start changing your discrim every hour, granted you can see someone with the same discrim
	#loop.create_task(autochangediscrim())
	await client.send_message(logchan, ":heart: Self bot up and running!")

@client.event
async def on_message(message):

	content = message.content

	if message.author == client.user: # Me only commands

		if content.lower().startswith('>purge'):
			params = content[6:].strip()
			await snailpurge(message, params)
			return

		if content.lower().startswith('>changediscrim'):
			await changediscrim(message)
			return

		if content.lower().startswith('>discrim'):
			params = content[8:].strip()
			await discrimfinder(message, params)
			return

		if content.lower().startswith('eval|'):
			code = content.lower()[5:].strip()
			await selfeval(message, code)

		if message.channel.is_private: 
			server = message.channel.id
		else:
			server = message.server.id

		if content.lower().startswith('>tag'):
			await handletag(message, content[4:].strip(), server)
			return

		if '>' not in content:
			return

		for i in tags['all']:
			if ">"+i['name'].lower() in content:
				content = content.replace(">"+i['name'], i['response'])
				await client.edit_message(message, content)

		if server in tags:
			for i in tags[server]:
				if ">"+i['name'].lower() in content:
					content = content.replace(">"+i['name'], i['response'])
					await client.edit_message(message, content)
		



client.run(config.token, bot=False)
